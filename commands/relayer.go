//usr/bin/env go run --race "${0}" "${@}"; exit ${?};
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/paul_john_king/relayer/relayer"
	"gitlab.com/paul_john_king/relayer/relayer/source"
	"gitlab.com/paul_john_king/relayer/relayer/target"
)

type (
	stringList []string
)

func (this *stringList) Set(str string) (err error) {
	*this = append(*this, str)

	return
}

func (this *stringList) String() (str string) {
	str = fmt.Sprintf("%v", *this)

	return
}

func getSubOptions(options []string) (boolOptions map[string]bool, stringOptions map[string]string) {
	boolOptions = make(map[string]bool)
	stringOptions = make(map[string]string)

	for _, option := range options {
		var (
			components []string
		)

		components = strings.SplitN(option, "=", 2)
		switch len(components) {
		case 1:
			s := strings.TrimPrefix(components[0], "no-")
			boolOptions[s] = s == components[0]
		case 2:
			stringOptions[components[0]] = components[1]
		}
	}

	return
}

func getFileName(path string) (fileName string) {
	fileName = path[strings.LastIndex(path, "/")+1:]

	return
}

func writeHelp(invocation string) {
	log.Printf(
		`Usage:

    %s [-h|--help] \
        [-q|--quiet] [-v|--verbose] \
        [-i|--interval «count»] [-r|--rest-port «port»] \
        [-s|--source-opt «opt» ...] [-t|--target-opt «opt» ...] \
        «source» «target»

`,
		invocation,
	)
}

func fail(invocation string, format string, args ...interface{}) {
	log.Printf("%s error: %s", invocation, fmt.Sprintf(format, args...))
	writeHelp(invocation)
	log.Fatal("Exiting")

	return
}

func main() {
	var (
		err                 error
		invocation          string
		help                bool
		quiet               bool
		verbose             bool
		verbosity           int
		interval            int
		restPort            int
		sourceOptions       stringList
		targetOptions       stringList
		sourceBoolOptions   map[string]bool
		sourceStringOptions map[string]string
		targetBoolOptions   map[string]bool
		targetStringOptions map[string]string
		sourceName          string
		targetName          string
		_source             relayer.Source
		_target             relayer.Target
	)

	log.SetOutput(os.Stdout)

	invocation = getFileName(os.Args[0])

	flag.Usage = func() { writeHelp(invocation) }
	flag.BoolVar(&help, "help", false, "")
	flag.BoolVar(&help, "h", false, "")
	flag.BoolVar(&quiet, "quiet", false, "")
	flag.BoolVar(&quiet, "q", false, "")
	flag.BoolVar(&verbose, "verbose", false, "")
	flag.BoolVar(&verbose, "v", false, "")
	flag.IntVar(&interval, "interval", 60, "")
	flag.IntVar(&interval, "i", 60, "")
	flag.IntVar(&restPort, "rest-port", 2525, "")
	flag.IntVar(&restPort, "r", 2525, "")
	flag.Var(&sourceOptions, "source-opt", "")
	flag.Var(&sourceOptions, "s", "")
	flag.Var(&targetOptions, "target-opt", "")
	flag.Var(&targetOptions, "t", "")
	flag.Parse()

	if help {
		flag.Usage()
		return
	}

	verbosity = 1
	if quiet {
		verbosity = 0
	}
	if verbose {
		verbosity = 2
	}

	sourceBoolOptions, sourceStringOptions = getSubOptions(sourceOptions)
	targetBoolOptions, targetStringOptions = getSubOptions(targetOptions)

	if flag.NArg() != 2 {
		fail(invocation, "'%s' requires 2 command-line arguments", invocation)
	}

	sourceName = flag.Arg(0)
	targetName = flag.Arg(1)

	switch sourceName {
	case "consul":
		var (
			ok   bool
			url  string
			mock bool
		)

		url, ok = sourceStringOptions["url"]
		if !ok {
			url = "http://localhost:9500"
		}
		mock, _ = sourceBoolOptions["mock"]

		_source, err = source.NewConsul(url, mock)
		if err != nil {
			fail(invocation, "I could not create a source with name '%s': %s", sourceName, err.Error())
		}
	default:
		fail(invocation, "'%s' is not a valid source name", sourceName)
	}

	switch targetName {
	case "prometheus":
		var (
			ok   bool
			url  string
			mock bool
		)

		url, ok = targetStringOptions["url"]
		if !ok {
			url = "http://localhost:9090"
		}
		mock, _ = targetBoolOptions["mock"]

		_target, err = target.NewPrometheus(url, mock)
		if err != nil {
			fail(invocation, "I could not create a target with name '%s': %s", targetName, err.Error())
		}
	default:
		fail(invocation, "'%s' is not a valid target name", targetName)
	}

	os.Exit(relayer.NewRelayer(invocation, verbosity, interval, restPort, _source, _target).Run())
}
