package relayer

import (
	"reflect"
	"strings"
)

type (
	Person struct {
		Name   string
		Sex    string
		Labels map[string]string
	}

	Datum struct {
		Name    string
		Members []*Person
	}

	Data map[string]*Datum
)

func (this *Data) String() (output string) {
	var (
		builder strings.Builder
	)

	for key, datum := range *this {
		builder.WriteString(key)
		builder.WriteString(":\n")

		builder.WriteString("  Name: ")
		builder.WriteString(datum.Name)
		builder.WriteString("\n")

		builder.WriteString("  Members:\n")
		for _, member := range datum.Members {
			builder.WriteString("    Name:")
			builder.WriteString(member.Name)
			builder.WriteString("\n")

			builder.WriteString("    Sex:")
			builder.WriteString(member.Sex)
			builder.WriteString("\n")

			builder.WriteString("    Labels:\n")
			for key, value := range member.Labels {
				builder.WriteString("      ")
				builder.WriteString(key)
				builder.WriteString(": ")
				builder.WriteString(value)
				builder.WriteString("\n")
			}
		}
	}

	output = builder.String()

	return
}

func (this *Data) Equals(data *Data) (ok bool) {
	ok = reflect.DeepEqual(this, data)

	return
}
