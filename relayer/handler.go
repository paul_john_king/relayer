package relayer

import (
	"os"
	"os/signal"
	"sync"
	"syscall"
)

type (
	SignalHandler interface {
		Start()
		Stop()
	}

	signalHandler struct {
		waiter                   *sync.WaitGroup
		logger                   Logger
		exitHandler              func(status int)
		increaseVerbosityHandler func()
		decreaseVerbosityHandler func()
		exitChannel              chan os.Signal
		increaseVerbosityChannel chan os.Signal
		decreaseVerbosityChannel chan os.Signal
	}
)

func NewSignalHandler(
	waiter *sync.WaitGroup,
	logger Logger,
	exitHandler func(status int),
	increaseVerbosityHandler func(),
	decreaseVerbosityHandler func(),
) (
	this *signalHandler,
) {
	this = new(signalHandler)

	this.waiter = waiter
	this.logger = logger
	this.exitHandler = exitHandler
	this.increaseVerbosityHandler = increaseVerbosityHandler
	this.decreaseVerbosityHandler = decreaseVerbosityHandler

	this.exitChannel = make(chan os.Signal)
	this.increaseVerbosityChannel = make(chan os.Signal)
	this.decreaseVerbosityChannel = make(chan os.Signal)

	return
}

func (this *signalHandler) Start() {
	this.logger.Info("Signal handler: I am starting")

	go func() {
		var (
			channelCount int
		)

		this.waiter.Add(1)

		signal.Notify(this.exitChannel, syscall.SIGINT, syscall.SIGTERM)
		signal.Notify(this.increaseVerbosityChannel, syscall.SIGUSR1)
		signal.Notify(this.decreaseVerbosityChannel, syscall.SIGUSR2)

		channelCount = 3

		for channelCount > 0 {
			select {
			case _, open := <-this.exitChannel:
				if open {
					this.exitHandler(0)
				} else {
					channelCount--
				}
			case _, open := <-this.increaseVerbosityChannel:
				if open {
					this.logger.IncreaseVerbosity()
				} else {
					channelCount--
				}
			case _, open := <-this.decreaseVerbosityChannel:
				if open {
					this.logger.DecreaseVerbosity()
				} else {
					channelCount--
				}
			}
		}

		this.logger.Info("Signal handler: I have stopped")

		this.waiter.Done()

		return
	}()

	return
}

func (this *signalHandler) Stop() {
	this.logger.Info("Signal handler: I am stopping")

	signal.Stop(this.exitChannel)
	signal.Stop(this.increaseVerbosityChannel)
	signal.Stop(this.decreaseVerbosityChannel)

	close(this.exitChannel)
	close(this.increaseVerbosityChannel)
	close(this.decreaseVerbosityChannel)

	return
}
