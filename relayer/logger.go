package relayer

import (
	"fmt"
	"log"
	"sync"
)

type (
	Logger interface {
		IncreaseVerbosity()
		DecreaseVerbosity()
		Err(format string, v ...interface{})
		Warning(format string, v ...interface{})
		Notice(format string, v ...interface{})
		Info(format string, v ...interface{})
		Debug(format string, v ...interface{})
	}

	logger struct {
		sync.Mutex
		invocation string
		verbosity  int
		_err       func(format string, v ...interface{})
		_warning   func(format string, v ...interface{})
		_notice    func(format string, v ...interface{})
		_info      func(format string, v ...interface{})
		_debug     func(format string, v ...interface{})
		_nop       func(format string, v ...interface{})
		err        func(format string, v ...interface{})
		warning    func(format string, v ...interface{})
		notice     func(format string, v ...interface{})
		info       func(format string, v ...interface{})
		debug      func(format string, v ...interface{})
	}
)

func NewLogger(invocation string, verbosity int) (this *logger) {
	this = new(logger)

	this.invocation = invocation
	this._err = this.logFactory("ERR")
	this._warning = this.logFactory("WARNING")
	this._notice = this.logFactory("NOTICE")
	this._info = this.logFactory("INFO")
	this._debug = this.logFactory("DEBUG")
	this._nop = func(format string, v ...interface{}) {}

	this.setVerbosity(verbosity)

	return
}

func (this *logger) logFactory(level string) func(format string, v ...interface{}) {
	return func(format string, v ...interface{}) {
		log.Printf("%s %s: %s", this.invocation, level, fmt.Sprintf(format, v...))

		return
	}
}

func (this *logger) setVerbosity(verbosity int) {
	switch {
	case verbosity <= 0:
		this.verbosity = 0
		this.err = this._err
		this.warning = this._warning
		this.notice = this._notice
		this.info = this._nop
		this.debug = this._nop
	case verbosity == 1:
		this.verbosity = 1
		this.err = this._err
		this.warning = this._warning
		this.notice = this._notice
		this.info = this._info
		this.debug = this._nop
	case verbosity >= 2:
		this.verbosity = 2
		this.err = this._err
		this.warning = this._warning
		this.notice = this._notice
		this.info = this._info
		this.debug = this._debug
	}

	return
}

func (this *logger) IncreaseVerbosity() {
	this.Lock()
	defer this.Unlock()

	this.setVerbosity(this.verbosity + 1)
}

func (this *logger) DecreaseVerbosity() {
	this.Lock()
	defer this.Unlock()

	this.setVerbosity(this.verbosity - 1)
}

func (this *logger) Err(format string, v ...interface{}) {
	this.Lock()
	defer this.Unlock()

	this.err(format, v...)
}

func (this *logger) Warning(format string, v ...interface{}) {
	this.Lock()
	defer this.Unlock()

	this.warning(format, v...)
}

func (this *logger) Notice(format string, v ...interface{}) {
	this.Lock()
	defer this.Unlock()

	this.notice(format, v...)
}

func (this *logger) Info(format string, v ...interface{}) {
	this.Lock()
	defer this.Unlock()

	this.info(format, v...)
}

func (this *logger) Debug(format string, v ...interface{}) {
	this.Lock()
	defer this.Unlock()

	this.debug(format, v...)
}
