package relayer

import (
	"sync"
	"time"
)

type (
	Relayer interface {
		Run() (exitStatus int)
	}

	relayer struct {
		waiter        *sync.WaitGroup
		exitChannel   chan int
		interval      time.Duration
		loop          bool
		logger        Logger
		signalHandler SignalHandler
		restServer    RestServer
		source        Source
		target        Target
		data          *Data
	}
)

func NewRelayer(
	invocation string,
	verbosity int,
	interval int,
	restPort int,
	source Source,
	target Target,
) (
	this *relayer,
) {
	this = new(relayer)

	this.waiter = new(sync.WaitGroup)
	this.exitChannel = make(chan int)
	this.interval = time.Duration(interval)
	this.loop = interval != 0
	this.logger = NewLogger(invocation, verbosity)
	this.signalHandler = NewSignalHandler(
		this.waiter,
		this.logger,
		this.exit,
		this.logger.IncreaseVerbosity,
		this.logger.DecreaseVerbosity,
	)
	this.restServer = NewRestServer(
		this.waiter,
		this.logger,
		restPort,
		this.exit,
	)
	this.source = source
	this.target = target

	return
}

func (this *relayer) Run() (exitStatus int) {
	var (
		err  error
		data *Data
	)

	this.logger.Notice("I am starting")

	this.signalHandler.Start()
	this.restServer.Start()

	this.logger.Notice("I have started")

	defer func() {
		this.logger.Notice("I am stopping")

		this.signalHandler.Stop()
		this.restServer.Stop()
		this.waiter.Wait()

		this.logger.Notice("I have stopped with exit status '%d'", exitStatus)

		return
	}()

	for {
		this.logger.Notice("I am waking")

		func() {
			data, err = this.source.ReadData(this.logger)
			if err != nil {
				this.logger.Err("My source cannot read the data from its service")
				return
			}

			this.logger.Debug(
				"I received the data\n----\n%s\n----\n",
				data.String(),
			)

			if data.Equals(this.data) {
				this.logger.Notice("The data has not changed")
				return
			}

			this.logger.Notice("The data has changed")

			err = this.target.WriteData(data, this.logger)
			if err != nil {
				this.logger.Err("My target cannot write the data to its service")
				return
			}

			err = this.target.Notify(this.logger)
			if err != nil {
				this.logger.Err("My target cannot be notified")
				return
			}

			this.data = data

			return
		}()

		if this.loop {
			this.logger.Notice("I am sleeping for %d seconds", this.interval)

			select {
			case _exitStatus := <-this.exitChannel:
				if exitStatus == 0 {
					exitStatus = _exitStatus
				}
				return
			case <-time.After(this.interval * time.Second):
				continue
			}
		} else {
			return
		}

	}
}

func (this *relayer) exit(exitStatus int) {
	this.exitChannel <- exitStatus

	return
}
