package relayer

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"
)

type (
	RestServer interface {
		Start()
		Stop()
	}

	restServer struct {
		waiter      *sync.WaitGroup
		logger      Logger
		exitHandler func(status int)
		server      *http.Server
	}
)

func NewRestServer(
	waiter *sync.WaitGroup,
	logger Logger,
	restPort int,
	exitHandler func(status int),
) (
	this *restServer,
) {
	this = new(restServer)

	this.waiter = waiter
	this.logger = logger
	this.exitHandler = exitHandler
	this.server = new(http.Server)
	this.server.Addr = fmt.Sprintf(":%d", restPort)

	http.HandleFunc("/hello", helloWorldHandler)
	http.HandleFunc("/goodbye", goodbyeWorldHandler)

	return
}

func (this *restServer) Start() {
	this.logger.Info("REST server: I am starting")

	go func() {
		var (
			err error
		)

		this.waiter.Add(1)

		err = this.server.ListenAndServe()
		switch {
		case err == http.ErrServerClosed:
			this.logger.Info("REST server: I have stopped")
		case err != nil:
			this.logger.Err("REST server: I could not start: %s", err.Error())

			this.exitHandler(1)
		}

		return
	}()

	return
}

func (this *restServer) Stop() {
	this.logger.Info("REST server: I am stopping")

	this.server.Shutdown(context.Background())
	this.waiter.Done()

	return
}

func helloWorldHandler(response http.ResponseWriter, request *http.Request) {
	http.ServeContent(
		response,
		request,
		request.URL.Path,
		time.Now(),
		strings.NewReader("Hello, World!"),
	)

	return
}

func goodbyeWorldHandler(response http.ResponseWriter, request *http.Request) {
	http.ServeContent(
		response,
		request,
		request.URL.Path,
		time.Now(),
		strings.NewReader("Goodbye, cruel World!"),
	)

	return
}
