package relayer

type Source interface {
	ReadData(logger Logger) (data *Data, err error)
}
