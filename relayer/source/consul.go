package source

import (
	"fmt"

	"gitlab.com/paul_john_king/relayer/relayer"
)

type (
	consul struct {
		url string

		_ReadData func(logger relayer.Logger) (data *relayer.Data, err error)
	}
)

func NewConsul(url string, mock bool) (this *consul, err error) {
	this = new(consul)

	this.url = url

	if mock {
		this._ReadData = func(logger relayer.Logger) (data *relayer.Data, err error) {
			data, err = this.mockReadData(logger)

			return
		}
	} else {
		this._ReadData = func(logger relayer.Logger) (data *relayer.Data, err error) {
			data, err = this.realReadData(logger)

			return
		}
	}

	return
}

func (this *consul) ReadData(logger relayer.Logger) (data *relayer.Data, err error) {
	logger.Info(
		"Consul source: I will try to read the data from the Consul service at '%s'",
		this.url,
	)

	data, err = this._ReadData(logger)
	if err != nil {
		return
	}

	logger.Info(
		"Consul source: I have read the data from the Consul service at '%s'",
		this.url,
	)

	return
}

func (this *consul) realReadData(logger relayer.Logger) (data *relayer.Data, err error) {
	err = fmt.Errorf("'realReadData' is not yet implemented")

	if err != nil {
		logger.Err(
			"Consul source: I cannot read the data from the Consul service at '%s': %s",
			this.url,
			err.Error(),
		)
		return
	}

	return
}

func (this *consul) mockReadData(logger relayer.Logger) (data *relayer.Data, err error) {
	var (
		barney *relayer.Person
		betty  *relayer.Person
		rubble *relayer.Datum
	)

	barney = new(relayer.Person)
	*barney = relayer.Person{
		Name: "Barney",
		Sex:  "male",
		Labels: map[string]string{
			"hobby": "bowling",
			"job":   "mining",
			"wife":  "Betty",
		},
	}

	betty = new(relayer.Person)
	*betty = relayer.Person{
		Name: "Betty",
		Sex:  "female",
		Labels: map[string]string{
			"husband": "Barney",
		},
	}

	rubble = new(relayer.Datum)
	*rubble = relayer.Datum{
		Name:    "Rubble",
		Members: []*relayer.Person{barney, betty},
	}

	data = new(relayer.Data)

	*data = relayer.Data{
		"rubble": rubble,
		"flintstone": &relayer.Datum{
			Name: "Flintstone",
			Members: []*relayer.Person{
				&relayer.Person{
					Name: "Fred",
					Sex:  "male",
					Labels: map[string]string{
						"hobby": "bowling",
						"job":   "mining",
						"wife":  "Wilma",
					},
				},
				&relayer.Person{
					Name: "Wilma",
					Sex:  "female",
					Labels: map[string]string{
						"husband": "Fred",
					},
				},
			},
		},
	}

	return
}
