package relayer

type Target interface {
	WriteData(data *Data, logger Logger) (err error)
	Notify(logger Logger) (err error)
}
