package target

import (
	"net/http"

	"gitlab.com/paul_john_king/relayer/relayer"
)

type (
	prometheus struct {
		url string

		_WriteData func(data *relayer.Data, logger relayer.Logger) (err error)
		_Notify    func(logger relayer.Logger) (err error)
	}
)

func NewPrometheus(url string, mock bool) (this *prometheus, err error) {
	this = new(prometheus)

	this.url = url

	if mock {
		this._WriteData = func(data *relayer.Data, logger relayer.Logger) (err error) {
			err = this.mockWriteData(data, logger)

			return
		}
		this._Notify = func(logger relayer.Logger) (err error) {
			err = this.mockNotify(logger)

			return
		}
	} else {
		this._WriteData = func(data *relayer.Data, logger relayer.Logger) (err error) {
			err = this.realWriteData(data, logger)

			return
		}
		this._Notify = func(logger relayer.Logger) (err error) {
			err = this.realNotify(logger)

			return
		}
	}

	return
}

func (this *prometheus) WriteData(data *relayer.Data, logger relayer.Logger) (err error) {
	logger.Info(
		"Prometheus target: I will try to write the data to the Prometheus service at '%s'",
		this.url,
	)

	err = this._WriteData(data, logger)
	if err != nil {
		return
	}

	logger.Info(
		"Prometheus target: I have written the data to the Prometheus service at '%s'",
		this.url,
	)

	return
}

func (this *prometheus) realWriteData(data *relayer.Data, logger relayer.Logger) (err error) {
	return
}

func (this *prometheus) mockWriteData(data *relayer.Data, logger relayer.Logger) (err error) {
	logger.Debug(
		"Prometheus target: I received the data\n----\n%s\n----\n",
		data.String(),
	)

	return
}

func (this *prometheus) Notify(logger relayer.Logger) (err error) {
	logger.Info(
		"Prometheus target: I will try to notify the Prometheus service at '%s'",
		this.url,
	)

	err = this._Notify(logger)
	if err != nil {
		return
	}

	logger.Info(
		"Prometheus target: I have notified the Prometheus service at '%s'",
		this.url,
	)

	return
}

func (this *prometheus) realNotify(logger relayer.Logger) (err error) {
	var (
		response *http.Response
	)

	response, err = http.Post(this.url+"/-/reload", "", nil)
	if err != nil {
		logger.Err(
			"Prometheus target: The Prometheus server at '%s' cannot reload its configuration file: %s",
			this.url,
			err.Error(),
		)
		return
	}
	defer func() {
		var (
			_err error
		)

		_err = response.Body.Close()
		if err == nil {
			err = _err
		}
		if _err != nil {
			logger.Err(
				"Prometheus target: I cannot close the body of the HTTP response from the Prometheus server at '%s': %s",
				this.url,
				_err.Error(),
			)
			return
		}

		return
	}()
	if response.StatusCode != 200 {
		logger.Err(
			"Prometheus target: The Prometheus server at '%s' cannot reload its configuration file: I received the HTTP status '%s'\n",
			this.url,
			response.Status,
		)
		return
	}

	return
}

func (this *prometheus) mockNotify(logger relayer.Logger) (err error) {

	return
}
